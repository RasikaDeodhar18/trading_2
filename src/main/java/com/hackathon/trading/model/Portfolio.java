package com.hackathon.trading.model;

import java.util.List;

public class Portfolio {

    //Stocks sold
    private List<Trade> history;

    //Stocks bought
    private List<Trade> currentStock; 
    
    private double currentValue;

    /**
     * @return the history
     */
    public List<Trade> getHistory() {
        return history;
    }

    /**
     * @param history the history to set
     */
    public void setHistory(List<Trade> history) {
        this.history = history;
    }

    /**
     * @return the currentStock
     */
    public List<Trade> getCurrentStock() {
        return currentStock;
    }

    /**
     * @param currentStock the currentStock to set
     */
    public void setCurrentStock(List<Trade> currentStock) {
        this.currentStock = currentStock;
    }

    /**
     * @return the currentValue
     */
    public double getCurrentValue() {
        return currentValue;
    }

    /**
     * @param currentValue the currentValue to set
     */
    public void setCurrentValue(double currentValue) {
        this.currentValue = currentValue;
    }





    
}
