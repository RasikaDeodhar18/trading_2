package com.hackathon.trading.model;

import java.util.Date;

public class Trade {

    private String id;
    private String name;
    private int quantity;
    private double price;
    
    //TRADE SPECIFICS
    private Date tradeStarted = new Date(System.currentTimeMillis());
    private TradeType tradeType;
    private TradeState tradeState = TradeState.CREATED;


    /**
     * Default constructor
     */
    public Trade (){

    }

    /**
     * Constructor with ARGS
     * @param id
     * @param name
     * @param quantity
     * @param price
     * @param tradeStarted
     * @param tradeType
     * @param tradeState
     */
    public Trade(String id, String name, int quantity, double price, TradeType tradeType,
            TradeState tradeState) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.settradeStarted(new Date(System.currentTimeMillis()));
        this.tradeType = tradeType;
        this.setTradeState(TradeState.CREATED);;
    }

    

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the tradeStarted
     */
    public Date gettradeStarted() {
        return tradeStarted;
    }

    /**
     * @param tradeStarted the tradeStarted to set
     */
    public void settradeStarted(Date tradeStarted) {
        this.tradeStarted = tradeStarted;
    }

    /**
     * @return the tradeType
     */
    public TradeType getTradeType() {
        return tradeType;
    }

    /**
     * @param tradeType the tradeType to set
     */
    public void setTradeType(TradeType tradeType) {
        this.tradeType = tradeType;
    }

    /**
     * @return the tradeState
     */
    public TradeState getTradeState() {
        return tradeState;
    }

    /**
     * @param tradeState the tradeState to set
     */
    public void setTradeState(TradeState tradeState) {
        this.tradeState = tradeState;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */

    @Override
    public String toString() {
        return "Trade [id=" + id + ", name=" + name + ", price=" + price + ", quantity=" + quantity + ", tradeState="
                + tradeState + ", tradeType=" + tradeType + ", tradeStarted=" + tradeStarted + "]";
    }

    

    
}
