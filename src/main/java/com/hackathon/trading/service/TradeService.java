package com.hackathon.trading.service;

import java.util.List;

import com.hackathon.trading.dao.TradeMongoRepo;
import com.hackathon.trading.model.Trade;
import com.hackathon.trading.model.TradeType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TradeService {

    @Autowired
    private TradeMongoRepo tradeMongoRepo;


    /**
     * Retrieve all trades
     * @param id
     * @param state
     */
    public List<Trade> retrieveTrades() {
        return tradeMongoRepo.findAll();
    }

    /**
     * Create/Insert trade
     * @param trade
     * @return
     */
    public Trade requestTrade(Trade trade) {
        return tradeMongoRepo.save(trade);
    }

    /**
     * Delete trade if not CREATED, FILLED, PENDING OR CANCELLED
     * @param id
     * @param state
     * @return 
     */
    public void deleteTrade(String id) {

        tradeMongoRepo.deleteById(id);

    }

    /**
     * Sell trade
     * @param trade
     * @return
     */
    public Trade sellTrade(Trade trade){
        trade.setTradeType(TradeType.SELL);

        return tradeMongoRepo.save(trade);
    }


    

}
