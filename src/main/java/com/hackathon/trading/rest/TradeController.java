package com.hackathon.trading.rest;

import java.util.List;

import com.hackathon.trading.model.Trade;
import com.hackathon.trading.model.TradeState;
import com.hackathon.trading.service.TradeService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin("*")
@RestController
@RequestMapping("v1/trade")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

    @Autowired
    private TradeService tradeService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Trade> retriveAllTrades() {
        LOG.debug("Get Trades Request.");
        return tradeService.retrieveTrades();
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Trade> requestTrades(@RequestBody Trade trade) {
        LOG.debug("Post Trades Request.");
        return new ResponseEntity<Trade>(tradeService.requestTrade(trade), HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<Trade> deleteTrades(@RequestBody Trade trade) {
        LOG.debug("Delete trades Request.");

        if(trade.getTradeState().equals(TradeState.CREATED)){

            tradeService.deleteTrade(trade.getId());

            return new ResponseEntity<Trade>(HttpStatus.CREATED);

        } else {

            return new ResponseEntity<Trade>(HttpStatus.BAD_REQUEST);

        }
    }


}
