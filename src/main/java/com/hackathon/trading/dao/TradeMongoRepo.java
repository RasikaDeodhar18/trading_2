package com.hackathon.trading.dao;

import com.hackathon.trading.model.Trade;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeMongoRepo extends MongoRepository<Trade, String> {

    // List<Trade> findById(String id);

}
