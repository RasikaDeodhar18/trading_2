package com.hackathon.trading.model;


import static org.junit.jupiter.api.Assertions.assertEquals;



import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TradeTests {

    private static Logger LOG = LoggerFactory.getLogger(TradeTests.class);

    Trade mockTrade = new Trade();
    String ticker = "AMZN", id = "mockId";
    int tradeQuantity = 10;
    double tradePrice = 250.50;
    TradeType buyTrade = TradeType.BUY;
    TradeState tradeCreated = TradeState.CREATED;

    @Test
    public void testConstructorWithArgs(){

        LOG.debug("RequestTrade() test");

        Trade tradeTest = new Trade();
        tradeTest.setId(id);
        tradeTest.setName(ticker);
        tradeTest.setQuantity(tradeQuantity);
        tradeTest.setPrice(tradePrice);
        tradeTest.setTradeType(buyTrade);
        tradeTest.setTradeState(tradeCreated);
        
        Trade tradeTestConsTest = new Trade(id, ticker, tradeQuantity, tradePrice, buyTrade, tradeCreated);

        assertEquals(tradeTest.toString(), tradeTestConsTest.toString());

    }    


    @Test
    public void testSetAndGet(){

        Trade tradeTest = new Trade();

        tradeTest.setId(id);
        assertEquals(tradeTest.getId(), id);

        tradeTest.setName(ticker);
        assertEquals(tradeTest.getName(), ticker);
    
        tradeTest.setQuantity(tradeQuantity);
        assertEquals(tradeTest.getQuantity(), tradeQuantity);

        tradeTest.setPrice(tradePrice);
        assertEquals(tradeTest.getPrice(), tradePrice);

        tradeTest.setTradeType(TradeType.BUY);
        assertEquals(tradeTest.getTradeType(), buyTrade);

        tradeTest.setTradeState(TradeState.CREATED);
        assertEquals(tradeTest.getTradeState(), tradeCreated);

    }


}
