package com.hackathon.trading.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.hackathon.trading.dao.TradeMongoRepo;
import com.hackathon.trading.model.Trade;
import com.hackathon.trading.model.TradeState;
import com.hackathon.trading.model.TradeType;
import com.hackathon.trading.service.TradeService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class TradeControllerTests {

    @Autowired
    private TradeService tradeService;

    @MockBean
    private TradeMongoRepo tradeMongoRepoTest;

    Trade mockTrade= new Trade();
    String ticker  = "AMZN", id = "mockId";
    int tradeQuantity = 10;
    double tradePrice = 250.50;
    TradeType tradeType = TradeType.BUY;;
    TradeState tradeState = TradeState.CREATED;
                                         

    @Test
    public void testTrade(){

        when(tradeMongoRepoTest.save(mockTrade)).thenReturn(mockTrade);
        Trade returned = tradeService.requestTrade(mockTrade);

        assert(mockTrade.toString().equals(returned.toString()));
     }



    @Test 
    public void testSetGetTicker(){

        mockTrade.setName(ticker);
        assertEquals(ticker, mockTrade.getName());

    }

    @Test
    public void testConstructorValid(){

        Trade testTrade = new Trade(id, ticker, tradeQuantity, tradePrice, tradeType, tradeState);

        assertEquals(ticker, testTrade.getName());
        assertEquals(tradePrice, testTrade.getPrice());

    }
    
}
