package com.hackathon.trading.service;


import static org.mockito.Mockito.when;

import com.hackathon.trading.dao.TradeMongoRepo;
import com.hackathon.trading.model.Trade;
import com.hackathon.trading.model.TradeState;
import com.hackathon.trading.model.TradeType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class TradeServiceTests {

    private static Logger LOG = LoggerFactory.getLogger(TradeServiceTests.class);

    @Autowired
    private TradeService tradeService;

    @MockBean
    private TradeMongoRepo tradeMongoRepoTest;

    Trade mockTrade;
    String ticker, id;
    int tradeQuantity;
    double tradePrice;

    @BeforeEach
    public void setUp(){

        mockTrade = new Trade();
        id = "mockId";
        ticker = "AMZN";
        tradeQuantity = 10;
        tradePrice = 250.50;
        
    }

    @Test
    public void testTradeServiceRequestTrade(){

        LOG.debug("RequestTrade() test");

        Trade tradeTest = new Trade();
        tradeTest.setName(ticker);
        tradeTest.setQuantity(tradeQuantity);
        tradeTest.setPrice(tradePrice);
        tradeTest.setTradeType(TradeType.BUY);
        tradeTest.setTradeState(TradeState.CREATED);

        when(tradeMongoRepoTest.save(tradeTest)).thenReturn(tradeTest);
        tradeService.requestTrade(tradeTest);

    }    
}
